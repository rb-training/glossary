build-props:
	./bin/build_props.sh

build:
	./bin/build.sh

check:
	echo "Shell check not yet available. Skipping for now."
	#shellcheck bin/build.sh

test:
	./tests/test_artifact_contents.sh
