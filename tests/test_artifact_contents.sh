#! /usr/bin/env bash

contents=`cat artifact.txt`
contents_expected="Artifact contents here"

if [ "$contents" == "$contents_expected" ]; then
  echo "[success]: artifact test passed"
  echo "[info] Tests 1 of 1 passed."
  echo "[info] All tests pass. Well done!"

else 
    echo "[error]: artifact test failed. Expected \"$contents_expected\". Got \"$contents\"." 
    echo "[info] Tests 1 of 1 failed."
    echo "[info] Test failures detected."
    exit 1
fi
